/**
* Modular.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  autoCreatedAt:false,
  autoUpdatedAt:false,
  attributes: {
    name:{
      type:'string',
      unique:true,
      required:true
    },
    title:{
      type:'string',
      unique:true,
      required:true
    },
    url:{
      type:'string',
      required:true
    },
    file:{
      type:'string',
      required:true
    },
    script:{
      type:'string',
      required:true
    },
    state:{
      type:'integer',
      required:true
    },
    createTime:{
      type:'integer',
      required:true
    }
  }
};


/**
* User.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  autoCreatedAt:false,
  autoUpdatedAt:false,
  attributes: {
    username:{
      type:"string",
      require:true,
      unique:true
    },
    state:{
      type:'integer',
      required:true
    },
    lastLoginTime:{
      type:'integer',
      required:true
    },
    createTime:{
      type:'integer',
      required:true
    },
    role:{
      collection:'Role',
      through:'role_user'
    }
  }
};


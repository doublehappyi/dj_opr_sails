/**
 * RoleController
 *
 * @description :: Server-side logic for managing roles
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  list:function(req, res){
    Role.find().exec(function(err, roles){
      if(err){res.json(err)}
      res.json(roles);
    });
  }
};


/**
 * ModularController
 *
 * @description :: Server-side logic for managing modulars
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  list:function(req, res){
    Modular.find().exec(function(err, modulars){
      if(err){res.json(err)}
      res.json(modulars);
    });
  }
};


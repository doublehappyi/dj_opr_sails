/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  list:function(req, res){
    User.find().populate('role').exec(function(err, users){
      if(err){res.json(err)}
      res.json(users);
    });
  }
};


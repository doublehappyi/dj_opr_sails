CREATE DATABASE IF NOT EXISTS dj_opr DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
use dj_opr;

create table if not exists `user`(
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `username` VARCHAR(16) UNIQUE NOT NULL,
  `password` VARCHAR(16) DEFAULT '8888888',
  `state` INT(1) DEFAULT 0,
  `lastLoginTime` INT(10) DEFAULT 0,
  `createTime` INT(10) NOT NULL
);

insert into user values(1, 'admin', 'admin', 1, 1452362609, 1452362609), (2, 'yishuangxi', 'yishuangxi', 1, 1452362609, 1452362609), (3, 'guest', 'guest', 0, 1452362609, 1452362609);

#角色表
CREATE TABLE IF NOT EXISTS `role`(
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` VARCHAR(16) UNIQUE NOT NULL,
  `state` INT(1) DEFAULT 0,
  `createTime` INT(10) NOT NULL
);
insert into role values (1, '管理员', 1, 1452362409),(2, '搜索组', 1, 1452362609),(3, '游客', 1, 1452362509);

CREATE TABLE IF NOT EXISTS `modular`(
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` VARCHAR(32) UNIQUE NOT NULL,
  `title` VARCHAR(32) UNIQUE NOT NULL,
  `url` VARCHAR(128) DEFAULT NULL,
  `file` VARCHAR(128) DEFAULT NULL,
  `script` VARCHAR(128) DEFAULT NULL,
  `state` INT(1) DEFAULT 0,
  `createTime` INT(10) NOT NULL
);


insert into modular values(1, 'user', '用户管理', 'url_1', 'file_1', 'script_1', 1, 1452362509);
insert into modular values(2, 'role', '角色管理', 'url_2', 'file_2', 'script_2', 1, 1452365509);
insert into modular values(3, 'modular', '模块管理', 'url_3', 'file_3', 'script_3', 1, 1452368509);

CREATE TABLE `role_modular`(
  `roleId` INT(10) UNSIGNED NOT NULL,
  `modularId` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`roleId`, `modularId`)
);

insert into `role_modular` values (1, 1),(1, 2),(1, 3),(2, 1),(2, 2), (3, 3);

CREATE TABLE `role_user`(
  `roleId` INT(10) UNSIGNED NOT NULL,
  `userId` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`roleId`, `userId`)
);
insert into `role_user` values (1, 1),(2, 2),(3, 3);

